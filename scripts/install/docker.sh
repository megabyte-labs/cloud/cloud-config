#!/usr/bin/env bash

# Determine whether or not to use sudo for installation
if ! type sudo &> /dev/null || [ "$USER" == 'root' ]; then
  SUDO_PREFIX=""
else
  SUDO_PREFIX="sudo"
fi

# Install Docker
if ! type docker &> /dev/null; then
  if [ -f '/etc/ubuntu-release' ] || [ -f '/etc/debian_version' ] || [ -f '/etc/redhat-release' ] || [ -f '/etc/SuSE-release' ]; then
    # Debian/RedHat/OpenSUSE/Ubuntu
    curl -fsSL https://get.docker.com | $SUDO_PREFIX sh
    $SUDO_PREFIX addgroup "$USER" docker
  elif [ -f '/etc/arch-release']; then
    # ArchLinux
    $SUDO_PREFIX pacman -Sy docker
    $SUDO_PREFIX addgroup "$USER" docker
    $SUDO_PREFIX systemctl start docker
    $SUDO_PREFIX systemctl enable docker
  elif [ -f '/etc/alpine-release' ]; then
    # Alpine
    $SUDO_PREFIX apk add --update docker docker-compose openrc
    $SUDO_PREFIX addgroup "$USER" docker
    $SUDO_PREFIX rc-update add docker boot
    $SUDO_PREFIX service docker start
  elif [ -d '/Applications'] && [ -d '/Library' ] && [ -d '/Users' ]; then
    # macOS
    if type brew &> /dev/null; then
      # Installs Docker Desktop
      brew install --cask docker
      $SUDO_PREFIX addgroup "$USER" docker
    else
      echo "ERROR: brew must be installed!" && exit 1
    fi
  else
    echo "ERROR: Unsupported distribution" && exit 1
  fi
fi
