#!/usr/bin/env bash

# Determine whether or not to use sudo for installation
if ! type sudo &> /dev/null || [ "$USER" == 'root' ]; then
  SUDO_PREFIX=""
else
  SUDO_PREFIX="sudo"
fi

# Install the OnlyKey CLI
if ! type onlykey-cli &> /dev/null; then
  if [ -f '/etc/ubuntu-release' ]; then
    # Ubuntu
    $SUDO_PREFIX apt update && $SUDO_PREFIX apt upgrade -y
    $SUDO_PREFIX apt install -y python3-pip python3-tk libusb-1.0-0-dev libudev-dev wget
    pip3 install onlykey
    $SUDO_PREFIX wget https://raw.githubusercontent.com/trustcrypto/trustcrypto.github.io/pages/49-onlykey.rules -O '/etc/udev/rules.d/49-onlykey.rules'
    $SUDO_PREFIX udevadm control --reload-rules && udevadm trigger
  elif [ -f '/etc/debian_version' ]; then
    # Debian
    $SUDO_PREFIX apt update && $SUDO_PREFIX apt upgrade -y
    $SUDO_PREFIX apt install -y python3-pip python3-tk libusb-1.0-0-dev libudev-dev wget
    pip3 install onlykey
    $SUDO_PREFIX wget https://raw.githubusercontent.com/trustcrypto/trustcrypto.github.io/pages/49-onlykey.rules -O '/etc/udev/rules.d/49-onlykey.rules'
    $SUDO_PREFIX udevadm control --reload-rules && udevadm trigger
  elif [ -f '/etc/redhat-release' ]; then
    if type dnf &> /dev/null; then
      $SUDO_PREFIX dnf install -y python3-pip python3-devel python3-tkinter libusb-devel libudev-devel gcc redhat-rpm-config wget
      pip3 install onlykey
      $SUDO_PREFIX wget https://raw.githubusercontent.com/trustcrypto/trustcrypto.github.io/pages/49-onlykey.rules -O '/etc/udev/rules.d/49-onlykey.rules'
      $SUDO_PREFIX udevadm control --reload-rules && udevadm trigger
    elif type yum &> /dev/null; then
      $SUDO_PREFIX yum update -y
      $SUDO_PREFIX yum install -y python3-pip python3-devel python3-tk libusb-devel libudev-devel gcc redhat-rpm-config wget
      pip3 install onlykey
      $SUDO_PREFIX wget https://raw.githubusercontent.com/trustcrypto/trustcrypto.github.io/pages/49-onlykey.rules -O '/etc/udev/rules.d/49-onlykey.rules'
      $SUDO_PREFIX udevadm control --reload-rules && udevadm trigger
    else
      echo "ERROR: No package manager on RedHat system." && exit 1
    fi
  elif [ -f '/etc/arch-release']; then
    # ArchLinux
    $SUDO_PREFIX pacman -Sy git python3-setuptools python3 libusb python3-pip wget
    pip3 install onlykey
    $SUDO_PREFIX wget https://raw.githubusercontent.com/trustcrypto/trustcrypto.github.io/pages/49-onlykey.rules -O '/etc/udev/rules.d/49-onlykey.rules'
    $SUDO_PREFIX udevadm control --reload-rules && udevadm trigger
  elif [ -d '/Applications'] && [ -d '/Library' ] && [ -d '/Users' ]; then
    # macOS
    if type brew &> /dev/null; then
      brew install libusb
      pip3 install onlykey
    else
      echo "ERROR: brew must be installed!" && exit 1
    fi
  elif [ -f '/etc/alpine-release']; then
    echo "ERROR: Alpine is currently unsupported" && exit 1
  elif [ -f '/etc/SuSE-release' ]; then
    echo "ERROR: OpenSUSE is currently unsupported" && exit 1
  else
    echo "ERROR: Unsupported distribution" && exit 1
  fi
fi
