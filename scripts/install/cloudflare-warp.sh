#!/usr/bin/env bash

# Determine whether or not to use sudo for installation
if ! type sudo &> /dev/null || [ "$USER" == 'root' ]; then
  SUDO_PREFIX=""
else
  SUDO_PREFIX="sudo"
fi

# @description Install cloudflare-warp/warp-cli if available otherwise install wgcf
if ! type warp-cli &> /dev/null; then
  if [ -f '/etc/ubuntu-release' ] || [ -f '/etc/debian_version' ]; then
    # Debian/Ubuntu
    curl https://pkg.cloudflareclient.com/pubkey.gpg | $SUDO_PREFIX gpg --yes --dearmor --output /usr/share/keyrings/cloudflare-warp-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/cloudflare-warp-archive-keyring.gpg] https://pkg.cloudflareclient.com/ $(lsb_release -cs) main" | $SUDO_PREFIX tee /etc/apt/sources.list.d/cloudflare-client.list
    $SUDO_PREFIX apt update
    $SUDO_PREFIX apt install -y cloudflare-warp
  elif [ -f '/etc/redhat-release' ]; then
    # RHEL/CentOS/Fedora
    $SUDO_PREFIX rpm -ivh https://pkg.cloudflareclient.com/cloudflare-release-el8.rpm
    $SUDO_PREFIX yum install cloudflare-warp
  elif [ -f '/etc/arch-release']; then
    # ArchLinux
    $SUDO_PREFIX yay -Sy cloudflare-warp-bin
    $SUDO_PREFIX systemctl start warp-svc
  elif [ -f '/etc/SuSE-release' ]; then
    # OpenSUSE
    # TODO
  elif [ -f '/etc/alpine-release' ]; then
    # Alpine
    # Source: https://github.com/ProfessorManhattan/wgcf-docker
    $SUDO_PREFIX apk --no-cache add -f curl ca-certificates iproute2 net-tools iptables wireguard-tools openresolv
    GH_API_URL='https://api.github.com/repos/ViRb3/wgcf/releases/latest'
    DOWNLOAD_URL="$(curl -fsSL "$GH_API_URL" | grep 'browser_download_url' | cut -d'"' -f4 | grep 'linux_amd64')"
    $SUDO_PREFIX curl -LS "$DOWNLOAD_URL" -o '/usr/bin/wgcf'
  elif [ -d '/Applications'] && [ -d '/Library' ] && [ -d '/Users' ]; then
    # macOS
    if type brew &> /dev/null; then
      brew install --cask cloudflare-warp
    else
      echo "ERROR: brew must be installed!" && exit 1
    fi
  else
    echo "ERROR: Unsupported distribution" && exit 1
  fi
fi

# @description Query current connection status and register/connect
if type warp-cli &> /dev/null; then
  WARP_STATUS="$(warp-cli --accept-tos status | grep 'Registration missing')"
  if [ -n "$WARP_STATUS" ]; then
    # Register
    warp-cli --accept-tos register
    # Connect the client
    warp-cli --accept-tos connect
    # Enroll in teams
    if [ -n "CLOUDFLARE_TEAMS_CLIENT_ID" ] && [ -n "$CLOUDFLARE_TEAMS_CLIENT_SECRET" ]; then
      warp-cli teams-enroll "$CLOUDFLARE_TEAMS_CLIENT_ID" "$CLOUDFLARE_TEAMS_CLIENT_SECRET"
    else
      # TODO -- do we need a team name here?
      warp-cli teams-enroll
    fi
  fi
  # WARP+DOH - WARP mode and DNS over HTTPS
  warp-cli set-mode warp+doh
elif type wgcf &> /dev/null; then
  # TODO - Complete set-up for distributions that rely on wgcf instead of warp-cli
else
  echo "ERROR: warp-cli or wgcf must be installed!"
fi
