#!/usr/bin/env bash

# Source: https://github.com/Paul-Reed/cloudflare-ufw

# Exit script if ufw is not installed
if ! type ufw &> /dev/null; then
  echo "ERROR: ufw must be present on the system!" && exit 1
fi

# Determine whether or not to use sudo for installation
if ! type sudo &> /dev/null || [ "$USER" == 'root' ]; then
  SUDO_PREFIX=""
else
  SUDO_PREFIX="sudo"
fi

# Create a list of CloudFlare IP ranges
TMP="$(mktemp)"
curl -s https://www.cloudflare.com/ips-v4 > "$TMP"
echo "" >> "$TMP"
curl -s https://www.cloudflare.com/ips-v6 >> "$TMP"

# Allow HTTPS and HTTP traffic from CloudFlare IPs
for CLOUDFLARE_IP in `cat $TMP`; do
  $SUDO_PREFIX ufw allow proto tcp from $CLOUDFLARE_IP to any port 80,443 comment 'CloudFlare IP'
done

# Reload the firewall
$SUDO_PREFIX ufw reload > /dev/null
