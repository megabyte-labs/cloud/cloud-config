"""
This template is written by @the-unknown
What does this quickstart script aim to do?
- This is my template which includes the new QS system.
  It includes a randomizer for my hashtags... with every run, it selects 10
  random hashtags from the list.
NOTES:
- I am using the bot headless on my vServer and proxy into a Raspberry PI I
have at home, to always use my home IP to connect to Instagram.
  In my comments, I always ask for feedback, use more than 4 words and
  always have emojis.
  My comments work very well, as I get a lot of feedback to my posts and
  profile visits since I use this tactic.
  As I target mainly active accounts, I use two unfollow methods. 
  The first will unfollow everyone who did not follow back within 12h.
  The second one will unfollow the followers within 24h.
  Source: https://github.com/InstaPy/instapy-quickstart/blob/master/quickstart_templates/good_commenting_strategy_and_new_qs_system.py
"""

# !/usr/bin/python2.7
import os
import random
from instapy import InstaPy
from instapy import smart_run

# get a session!
session = InstaPy(
    os.getenv("INSTAPY_USERNAME"), os.getenv("INSTAPY_PASSWORD"), headless_browser=True
)

# let's go! :>
with smart_run(session):
    hashtags = [
        "ai",
        "ansible",
        "apps",
        "bravebrowser",
        "chrome",
        "cybersecurity",
        "devops",
        "firefox",
        "fullstack",
        "git",
        "github",
        "gitlab",
        "golang",
        "hacker",
        "hashicorp",
        "homelab",
        "iot",
        "k8s",
        "kalilinux",
        "linux",
        "linuxfan",
        "macos",
        "makeuseof",
        "microsoft",
        "microsoft",
        "opensource",
        "operatingsystem",
        "packer",
        "producthunt",
        "python",
        "qubes",
        "qubesos",
        "slashdot",
        "software",
        "softwaredeveloper",
        "stackoverflow",
        "supplychain",
        "sysadmin",
        "techcrunch",
        "webapp",
        "webdev",
        "website",
        "websitedevelopment",
        "windows11",
    ]
    random.shuffle(hashtags)
    my_hashtags = hashtags[:10]

    # general settings
    session.set_dont_like(["sad", "rain", "depression"])
    session.set_do_follow(enabled=True, percentage=80, times=1)
    session.set_do_comment(enabled=True, percentage=80)
    session.set_comments(
        [
            "What an amazing post! :heart_eyes: What do "
            "you think of my recent post? #somewhatautomated",
            "What an amazing post! :heart_eyes: I think "
            "you might also like mine. :wink: #somewhatautomated",
            "Wonderful!! :heart_eyes: Would be awesome if "
            "you would checkout my posts as well! #somewhatautomated",
            "Wonderful!! :heart_eyes: I would be honored "
            "if you would checkout my posts and tell me "
            "what you think. :wink: #fromcomputer",
            "This is awesome!! :heart_eyes: Any feedback "
            "for my posts? :wink: #automation",
            "This is awesome!! :heart_eyes:  maybe you "
            "like my posts, too? :wink: #mycomputerwrotethis",
            "I really like the way you captured this. I "
            "bet you like my posts, too :wink: #mycomputerwrotethis",
            "I really like the way you captured this. If "
            "you have time, check out my posts, too. I "
            "bet you will like them. :wink: #wheredemgirlsat #autobot",
            "Great capture!! :smiley: Any feedback for my "
            "recent post? :wink: #somewhatautomated",
            "Great capture!! :smiley: :thumbsup: What do "
            "you think of my recent post? #dontclickimabot #jk #notjk",
        ],
        media="Photo",
    )
    session.set_do_like(True, percentage=70)
    session.set_delimit_liking(enabled=True, max_likes=100, min_likes=0)
    session.set_delimit_commenting(enabled=True, max_comments=20, min_comments=0)
    session.set_relationship_bounds(
        enabled=True,
        potency_ratio=None,
        delimit_by_numbers=True,
        max_followers=3000,
        max_following=2000,
        min_followers=50,
        min_following=50,
    )

    session.set_quota_supervisor(
        enabled=True,
        sleep_after=["likes", "follows"],
        sleepyhead=True,
        stochastic_flow=True,
        notify_me=True,
        peak_likes_hourly=200,
        peak_likes_daily=585,
        peak_comments_hourly=80,
        peak_comments_daily=182,
        peak_follows_hourly=48,
        peak_follows_daily=None,
        peak_unfollows_hourly=35,
        peak_unfollows_daily=402,
        peak_server_calls_hourly=None,
        peak_server_calls_daily=4700,
    )

    session.set_user_interact(amount=10, randomize=True, percentage=80)

    # activity
    session.like_by_tags(my_hashtags, amount=90, media=None)
    session.unfollow_users(
        amount=500,
        instapy_followed_enabled=True,
        instapy_followed_param="nonfollowers",
        style="FIFO",
        unfollow_after=24 * 60 * 60,
        sleep_delay=501,
    )
    session.unfollow_users(
        amount=500,
        instapy_followed_enabled=True,
        instapy_followed_param="all",
        style="FIFO",
        unfollow_after=48 * 60 * 60,
        sleep_delay=501,
    )

    """ Joining Engagement Pods...
    """
    session.join_pods(topic=os.getenv("INSTAPY_POD"), engagement_mode="no_comments")
