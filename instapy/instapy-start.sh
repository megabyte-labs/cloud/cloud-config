#!/usr/bin/env sh

wget "${INSTAPY_CONFIG_URL:-https://gitlab.com/megabyte-labs/cloud/cloud-config/-/raw/master/instapy/default.py}" -O instapy-routine.py
python instapy-routine.py
