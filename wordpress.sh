#!/usr/bin/env bash

# Ensure headless
export DEBIAN_FRONTEND=noninteractive

# Options - keep all typically customized variables up here
ADMIN_EMAIL="brian@megabyte.space"
ADMIN_PASS="HeyYoHDR1" # IMPORTANT NOTE: Change password after logging in since this config is public
ADMIN_USER="hdri"
FQDN="hdri.do.megabyte.space"
PLUGINS_ACTIVATED="akismet all-in-one-seo-pack broken-link-checker google-site-kit redirection tinymce-advanced ultimate-addons-for-gutenberg updraftplus web-stories worker wordfence wp-mail-smtp wp-optimize wp-redis"
PLUGINS_INSTALLED_ONLY="advanced-custom-fields amp bbpress code-snippets onesignal-free-web-push-notifications regenerate-thumbnails shortcodes-ultimate tablepress woocommerce wordpress-social-login wp-help wp-maintenance-mode"
SITE_DESCRIPTION="The Human Dirofilariasis Research Institute"
SITE_EMAIL="no-reply@hdri.org"
SITE_TIMEZONE="America/New_York"
SITE_TITLE="HDRI"
THEME="https://open.nyc3.digitaloceanspaces.com/wp/dgt-soraka.zip"

# Install EasyEngine
wget -qO ee rt.cx/ee4
DEBIAN_FRONTEND=noninteractive sudo bash ee

# Set up single instance of WordPress with HTTPS and Redis caching (use proxy-cache for high traffic websites)
ee site create $FQDN --type=wp --admin-email="$ADMIN_EMAIL" --admin-pass="$ADMIN_PASS" --admin-user="$ADMIN_USER" --cache --proxy-cache=off --ssl=le --title="$SITE_TITLE" --yes --le-mail="$SITE_EMAIL" --wp-mail="$SITE_EMAIL"

# Install and activate theme
ee shell $FQDN --command="wp theme install '$THEME' --activate"

# Install plugins
ee shell $FQDN --command="wp plugin install $PLUGINS_ACTIVATED"
ee shell $FQDN --command="wp plugin activate $PLUGINS_ACTIVATED"
ee shell $FQDN --command="wp plugin install $PLUGINS_INSTALLED_ONLY"

# Pre-configure options - please open a PR or Issue if you have any recommendations for
# automating other options
# TODO: Automatically add GA tag defined in top to MonsterInsights
# TODO: Automatically install and activate required plugins by theme
# TODO: Automatically set logo (assume twentytwentytwo is being used) and add comment here about recommended size
ee shell $FQDN --command="wp option update time_format 'H:i'"
ee shell $FQDN --command="wp option update date_format 'Y-m-d'"
ee shell $FQDN --command="wp option update blogdescription '$SITE_DESCRIPTION'"
ee shell $FQDN --command="wp option update category_base '/category'"
ee shell $FQDN --command="wp option update tag_base '/tag'"
ee shell $FQDN --command="wp option update timezone_string '$SITE_TIMEZONE'"

# Enable firewall - block everything except HTTPS and custom SSH port
ufw allow proto tcp to 0.0.0.0/0 port 443
ufw allow proto tcp to 0.0.0.0/0 port 2214
ufw enable

# Harden SSH
sed -i -e '/^#Port/s/^.*$/Port 2214/' /etc/ssh/sshd_config
# TODO: Merge wordpress-cloud-config.yml functionality (i.e. adding the user / SSH key) and then uncomment these lines
#sed -i -e '/^PermitRootLogin/s/^.*$/PermitRootLogin no/' /etc/ssh/sshd_config
#sed -i -e '$aAllowUsers hdri' /etc/ssh/sshd_config
service ssh restart

# TODO: Install and configure fail2ban - See: https://rtcamp.com/tutorials/nginx/fail2ban/

# Reboot
reboot
